# A simple Golang template renderer with [sprig](https://github.com/Masterminds/sprig) template extensions

![logo](injector-small.png)

Not every software which is deliverd via Docker containers is able to use environment variables directly in the configuration files. But these environment variables are crucial to control the software. Usually scripts are used to take these environment variable values and glue them into the configuration files. But this is not very flexible because different configuration files are using different configuration languages (jaml, json, ...).

Injector now takes a [Golang template file](https://golang.org/pkg/text/template/) and the whole list of environment variables puts them together an prints the result on the standard output.

# Here is an example with the Corefile.tpl from the CoreDNS:

~~~
. {
    etcd local {
        stubzones
        path {{ .MY_ZONE_PATH }}
        endpoint https://{{ .MY_ETCD }}:2379
        tls /coredns.pem /coredns-key.pem /ca.pem
    }
    bind {{ .MY_IP }}
    {{ if hasKey $ "LOG" }}log
    {{ end -}}
    errors
    prometheus {{ .MY_IP }}:29153
    loadbalance
}
~~~

# Run the Corefile.tpl with the `injector`

~~~bash
MY_IP=1.1.1.1 MY_ETCD=2.2.2.2 MY_ZONE_PATH=/D/S/F LOG= injector render /tmp/Corefile.tpl 
. {
    etcd local {
        stubzones
        path /D/S/F
        endpoint https://2.2.2.2:2379
        tls /coredns.pem /coredns-key.pem /ca.pem

    }
    bind 1.1.1.1
    log
    errors
    prometheus 1.1.1.1:29153
    loadbalance
}
~~~

Just redirect the output to the position where you need the configuration file. For example:

~~~bash
MY_IP=1.1.1.1 MY_ETCD=2.2.2.2 MY_ZONE_PATH=/D/S/F LOG= injector render /tmp/Corefile.tpl > /etc/coredns/Corefile
~~~

Put the `injector` into a self build Docker image and use it in your `entrypoint` script just before you start the daemon binary:

~~~bash
#!/bin/bash
injector render /tmp/Corefile.tpl > /etc/coredns/Corefile && exec /coredns
~~~

Cool, isn't it?

