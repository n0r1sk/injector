/*
Copyright 2017 Mario Kleinsasser and Bernhard Rausch

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"bufio"
	"bytes"
	"os"
	"path"
	"strings"

	"text/template"

	// logging
	log "github.com/sirupsen/logrus"

	// sprig template library
	"github.com/Masterminds/sprig"

	// commandline command and flag parsing
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

func showVersion() {
	log.Info("v0.1")
}

func runRender() {

	if *verbose {
		log.SetLevel(log.DebugLevel)
	}

	_, err := os.Stat(*renderarg)
	if err != nil {
		log.Fatal(err)
	}

	var dict map[string]interface{}
	dict = make(map[string]interface{})

	for _, e := range os.Environ() {
		pair := strings.Split(e, "=")
		key := (pair[0])
		value := (pair[1])
		log.Debug(key + " " + value)
		dict[key] = value
	}

	// process template
	var tpl bytes.Buffer

	// Render template
	name := path.Base(*renderarg)
	t, err := template.New(name).Funcs(sprig.TxtFuncMap()).ParseFiles(*renderarg)

	err = t.Execute(&tpl, dict)
	if err != nil {
		log.Fatal(err)
	}

	f := bufio.NewWriter(os.Stdout)
	defer f.Flush()
	f.Write([]byte(tpl.String()))
}

var (
	app       = kingpin.New("injector", "A command-line too render Golang templates")
	verbose   = app.Flag("verbose", "Enable verbose mode.").Bool()
	render    = app.Command("render", "Renders a golang template")
	renderarg = render.Arg("template", "Template (path) to render").Required().String()
	version   = app.Command("version", "shows the version of historian")
	lgs       = []string{}
)

func main() {

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case render.FullCommand():
		runRender()
	case version.FullCommand():
		showVersion()
	}

	// configure logrus logger
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)

}
